import { Component } from '@angular/core';

import {SpotifyService} from 'angular2-spotify';
@
Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',

  providers : [
    SpotifyService,
    { provide :"SpotifyConfig", useValue : {
        clientId: '173c8b2ee7b04277970f71108a9b368e',
        redirectUri: 'http://www.example.com/callback.html',
        scope: 'user-follow-modify user-follow-read playlist-read-private playlist-read-collaborative playlist-modify-public playlist-modify-private user-library-read user-library-modify user-read-private',
        authToken: localStorage.getItem('angular2-spotify-token')
      }
    }
  ]
})

export class HomePage {
  private user: Object
  constructor( private spotifyService: SpotifyService,) {
    this.getUser();
  }


  getUser() {
    this.spotifyService.getCurrentUser().subscriber(data => {
      console.log(data);
      this.user = data;
      this.userId = data.id;
      this.doTests();
    }, err=> {console.log(err);});
  }

  login() {
    this.spotifyService.login().subscribe(
      token => {
        console.log(token);
        this.getUser();
      },
      err => console.error(err),
      () => {}); 
  }

  doTests() {
   /* this.albumsTests();
    this.artistsTests();
    this.browseTests();
    this.followTests();
    this.libraryTests();
    this.personalizationTest();
    this.playlistTests();
    this.profilesTests();
    this.searchTests();
    this.trackTests();*/
}

userId;
/*trackUri = "spotify:track:49p3JJ7QnwHGqJG0vImRRl";
trackUri2 = "spotify:track:7cnsxfgpaF1tuXQDkjRs0m";
trackUri3 = "spotify:track:5h5tBFnbcVioFXiOixTn6E";
trackUri4 = "spotify:track:7bre6yd84LZ6MFoTppmHja";*/
}

